// TODO: 1.Using Promises

const uber = new Promise((resolve, reject) => {
  let arrived;
  if (arrived) {
    resolve('driver arrived');
  } else {
    reject('driver bailed');
  }
});

uber
  .then((ride) => {
    console.log(ride);
    //driver arrived
  })
  .catch((error) => {
    console.log(error);
    //driver bailed
  });

const makeServerRequest = new Promise((resolve, reject) => {
  let responseFromServer;
  if (responseFromServer) {
    resolve('We got the data');
  } else {
    reject('Data not received');
  }
});

makeServerRequest
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.log(error);
  });

// TODO: 2.Iterators and generators

// __Iterators =>
const dragons = ['cool dragon', 'angry dragon', 'nasty dragon'];

//iterator function
dragons[Symbol.iterator] = function () {
  let items = this;
  let step = 0;
  return {
    next() {
      let obj = {
        done: step >= items.length,
        value: items[step],
      };
      step++;
      return obj;
    },
  };
};

for (const dragon of dragons) {
  console.log(dragon);
}

// __Generators* =>

// Array->
function* heros() {
  console.log('process Batman');
  yield 'Batman';
  console.log('process SuperMan');
  yield 'SuperMan';
  console.log('process WonderWoman');
  yield 'WonderWoman';

  return 'DC Heros';
}

// __in next
let herosIt = heros();
console.log(herosIt.next());
// ....
console.log(herosIt.next());
// ....
console.log(herosIt.next());

// __in for
for (const hero of heros()) {
  console.log(hero);
}

// __[...]
console.log([...heros()]);

// Object->

let car = {
  make: 'Ford',
  model: 'Mustang',
  year: 1969,
  mpg: [
    {
      highway: '10 mpg',
      city: '15 mpg',
    },
  ],
  *[Symbol.iterator]() {
    yield* this.mpg;
  },
};

for (const details of car) {
  console.log(details);
}

// TODO: 3.JavaScript modules
/*Import / Export */

// !1.Named Exports
// in file___app.js____

export const sqrt = Math.sqrt;
export function square(num) {
  return num * num;
}
export function diag(x, y) {
  return sqrt(square(x) + square(y));
}

// !2.Import Part of a Module
// in file___main.js____
import { square, diag } from 'app';
console.log(square(11));
console.log(diag(4, 3));

// !3.Import Complete Module
// in file___main.js____
import * as app from 'app';
console.log(app.square(11));
console.log(app.diag(4, 3));

// !4.Importing with more convenient alias
// in file___main.js____
import { reallyReallyLongModuleMemberName as shortName } from 'my_module';

// !5.Single default Export
// in file___myFunc.js____
export default function () {}

// in file___lib.js___
import myFunc from 'myFunc';
myFunc();

// TODO: 4.Meta programming

// __proxy->
class User {
  constructor(username, password) {
    this.username = username;
    this.password = password;
  }
}
const userOne = new User('userOne', '****');
let handler = {
  get(obj, propKey) {
    return propKey in obj ? obj[propKey] : 'PropKey dasent exist';
  },
  set(obj, propKey, value) {
    if (typeof value === 'number') {
      Reflect.set(obj, propKey, value);
    }
  },
  // has()
};

const newProxy = new Proxy(userOne, handler);
console.log(newProxy.username);

// __reflect API->
class Car {
  constructor(make, model) {
    this.make = make;
    this.model = model;
  }
}
let myCar = new Car('Ford', 'Mustang');
let herCar = Reflect.construct(Car, ['Ferrari', 'F8']);
console.log(myCar);

let agent = {
  name: 'James Bond',
  agent: 007,
};
const agentSummery = (misson) => {
  return `Hello agent ${this.agent}: ${this.name} your misson in ${misson}`;
};
console.log(Reflect.apply(agentSummery, agent, ['Find agent 008']));

Reflect.defineProperty(agent, 'age', {
  value: 40,
}); //Object.define.property
